package com.olzindel.photoadhoken.ui.editphoto;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;

public class Sticker {
    private RectF rectF;
    private Bitmap bitmap;
    
    public Sticker(Bitmap bitmap) {
        this.bitmap = bitmap;
        this.rectF = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public RectF getRectF() {
        return rectF;
    }
}
