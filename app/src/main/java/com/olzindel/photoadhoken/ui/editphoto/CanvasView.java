package com.olzindel.photoadhoken.ui.editphoto;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;

public class CanvasView extends View implements SensorEventListener {
    final static private float DELTA_TO_CONSIDER_MOVEMENT = 30;
    public int i;
    private Paint defaultPaint;
    private Paint photoPaint;
    private Bitmap photo;
    private Matrix photoTransformation = new Matrix();
    private LinkedList<Sticker> stickers = new LinkedList<>();
    private Sticker movingSticker = null;
    private float lastX = 0;
    private float lastY = 0;
    private SensorManager sensorManager;

    public CanvasView(Context context) {
        super(context);
        init(context);
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        photo = Bitmap.createBitmap(1, 1, conf);
        defaultPaint = new Paint();
        defaultPaint.setFilterBitmap(true);
        photoPaint = new Paint();


        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int w = canvas.getClipBounds().width();
        int h = canvas.getClipBounds().height();
        canvas.drawBitmap(photo, null, new RectF(0, 0, w, h), photoPaint);

        for (final Sticker sticker : stickers) {
            canvas.drawBitmap(sticker.getBitmap(), null, sticker.getRectF(), defaultPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int eventaction = event.getAction();

        switch (eventaction) {
            case MotionEvent.ACTION_DOWN:

                for (Iterator<Sticker> it = stickers.descendingIterator(); it.hasNext(); ) {
                    final Sticker sticker = it.next();
                    if (sticker.getRectF().right > event.getX()
                            && sticker.getRectF().left < event.getX()
                            && sticker.getRectF().top < event.getY()
                            && sticker.getRectF().bottom > event.getY()) {
                        movingSticker = sticker;
                        lastX = event.getX();
                        lastY = event.getY();
                        break;
                    }
                }
                break;

            case MotionEvent.ACTION_MOVE:
                // finger moves on the screen
                if (movingSticker != null) {
                    if (Math.abs(event.getX() - lastX) >= DELTA_TO_CONSIDER_MOVEMENT || Math.abs(event.getY() - lastY) >= DELTA_TO_CONSIDER_MOVEMENT) {
                        final float deltaX = (movingSticker.getRectF().right - movingSticker.getRectF().left) / 2.f;
                        final float deltaY = (movingSticker.getRectF().bottom - movingSticker.getRectF().top) / 2.f;
                        movingSticker.getRectF().set(event.getX() - deltaX, event.getY() - deltaY, event.getX() + deltaX, event.getY() + deltaY);
                        lastX = event.getX();
                        lastY = event.getY();
                        invalidate();
                    }
                }
                break;

            case MotionEvent.ACTION_UP:
                movingSticker = null;
                break;
            default:
                break;
        }

        // tell the system that we handled the event and no further processing is required
        return true;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        setPhoto(this.photo);
    }

    public Bitmap saveImage() {
        return photo;
    }

    public void addSticker(final Sticker sticker) {
        sticker.getRectF().set(0, 0, 200, 200);
        stickers.addLast(sticker);
        invalidate();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_LIGHT:
                float lux = sensorEvent.values[0];
                int alpha = 256 - (int) lux * 100 * 128 / 10000;
                alpha = Math.min(alpha, 255);
                alpha = Math.max(alpha, 0);
                photoPaint.setAlpha(alpha);
                invalidate();
                break;
            case Sensor.TYPE_ACCELEROMETER:
                // Axis of the rotation sample, not normalized yet.
                double axisX = sensorEvent.values[0];
                double axisY = sensorEvent.values[1];
                double axisZ = sensorEvent.values[2];

                double norm_Of_g = Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

                axisX = axisX / norm_Of_g;
                axisY = axisY / norm_Of_g;
                axisZ = axisZ / norm_Of_g;

                int inclination = (int) Math.round(Math.toDegrees(Math.acos(axisX)));
                int inclinationToRed = Math.max(0xFF - inclination * 0xFF / 90, 0x00);
                int inclinationToBlue = Math.max(inclination * 0xFF / 90 - 0xFF, 0x00);

                int color = 0xFF << 8;
                color = (color + inclinationToRed) << 8;
                color = color << 8;
                color = color + inclinationToBlue;
                LightingColorFilter colorFilter = new LightingColorFilter(0xFFFFFFFF, color);

                photoPaint.setColorFilter(colorFilter);

                invalidate();
                break;
            default:
                break;
        }
    }

    public File getCurrentImage() throws IOException {
        Bitmap returnedBitMap;
        this.setDrawingCacheEnabled(true);
        this.buildDrawingCache(true);
        returnedBitMap = getDrawingCache();
        returnedBitMap = returnedBitMap.copy(Bitmap.Config.ARGB_8888, true);
        this.setDrawingCacheEnabled(false);

        final File file = File.createTempFile("image", ".jpeg");
        file.deleteOnExit();
        final OutputStream out = new FileOutputStream(file);

        returnedBitMap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        return file;
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
