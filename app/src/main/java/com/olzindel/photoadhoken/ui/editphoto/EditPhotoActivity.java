package com.olzindel.photoadhoken.ui.editphoto;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.olzindel.photoadhoken.R;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

public class EditPhotoActivity extends Activity {

    private CanvasView canvasView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.menu_editPhoto);
        setContentView(R.layout.activity_edit_photo);

        openPicture(getPathFromIntent());
    }

    private String getPathFromIntent() {
        return getIntent().getStringExtra("currentPicturePath");
    }

    private void openPicture(String path) {
        File picture = new File(path);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        Bitmap bitmap = BitmapFactory.decodeFile(picture.getAbsolutePath());

        int w = bitmap.getWidth();
        canvasView = findViewById(R.id.editPhotoView);
        canvasView.setPhoto(bitmap);

        boolean exited = picture.delete();
    }

    public void onClickSticker(View view) {
        if (view instanceof ImageView) {
            final ImageView imageView = (ImageView) view;
            if (imageView.getDrawable() instanceof BitmapDrawable) {
                final BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
                canvasView.addSticker(new Sticker(bitmapDrawable.getBitmap()));
            }
        }
    }


    public void onClickSaveEditedPhoto(View view) throws IOException {
        File file = canvasView.getCurrentImage();
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://photo-adhoken.appspot.com/");
        firebaseAuth.signInAnonymously().addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                try {
                    savePhotoOnFirebase(file, mStorageRef);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void savePhotoOnFirebase(File file, StorageReference mStorageRef) throws IOException {

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(file.getPath());
            double latitude = Double.parseDouble(Objects.requireNonNull(getIntent().getStringExtra("currentLocationLatitude")));
            double longitude = Double.parseDouble(Objects.requireNonNull(getIntent().getStringExtra("currentLocationLongitude")));
            String[] degMinSec = Location.convert(latitude, Location.FORMAT_SECONDS).split(":");
            String[] degMaxSec = Location.convert(longitude, Location.FORMAT_SECONDS).split(":");

            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, degMinSec[0] + "/1," + degMinSec[1] + "/1," + degMinSec[2] + "/1000");
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, degMaxSec[0] + "/1," + degMaxSec[1] + "/1," + degMaxSec[2] + "/1000");

            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, latitude < 0 ? "S" : "N");
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, longitude < 0 ? "W" : "E");
            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        UploadTask uploadTask = mStorageRef.child(UUID.randomUUID() + ".jpeg").putFile(Uri.fromFile(file));
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getApplicationContext(), "File successfully updated", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("EDITVIEW", "C'est kc");
            }
        });
    }
}
