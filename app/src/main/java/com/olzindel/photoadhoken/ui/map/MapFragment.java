package com.olzindel.photoadhoken.ui.map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;
import com.olzindel.photoadhoken.BuildConfig;
import com.olzindel.photoadhoken.R;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;


public class MapFragment extends Fragment {

    private MapView mMapView;
    private StorageReference mStorageRef;
    private FirebaseAuth firebaseAuth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(this.getContext()), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE}, 200);
        }


        mMapView = new MapView(inflater.getContext());

        createLocation(getContext());
        downloadAllImages();
        return mMapView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.getOverlays().clear();
        createLocation(getContext());
        downloadAllImages();
    }

    private void addMarker(GeoPoint geoPoint, File image) {
        Marker marker = new Marker(mMapView);
        marker.setPosition(geoPoint);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        Drawable dr = getResources().getDrawable(R.drawable.ic_marker);
        Bitmap bitmap = getBitmapFromVectorDrawable(dr);
        Drawable icon = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 50, 50, true));
        marker.setIcon(icon);
        InputStream targetStream = null;
        try {
            targetStream = new FileInputStream(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        marker.setImage(Drawable.createFromStream(targetStream, image.getName()));
        mMapView.getOverlays().add(marker);
    }

    private Bitmap getBitmapFromVectorDrawable(Drawable drawable) {

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void downloadAllImages() {
        mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://photo-adhoken.appspot.com/");
        firebaseAuth.signInAnonymously().addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                try {
                    downloadImagesAndAddMarker();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
    }


    private void downloadImagesAndAddMarker() throws IOException {
        mStorageRef.listAll().addOnSuccessListener(new OnSuccessListener<ListResult>() {
            @Override
            public void onSuccess(ListResult listResult) {
                for (StorageReference fileRef : listResult.getItems()) {
                    try {
                        File localFile = File.createTempFile("temp-", ".jpeg");
                        localFile.deleteOnExit();

                        fileRef.getFile(localFile)
                                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                        try {
                                            ExifInterface exif = new ExifInterface(localFile.getAbsolutePath());
                                            float[] latLong = new float[2];
                                            boolean hasLatLong = exif.getLatLong(latLong);
                                            double lat = 0;
                                            double lng = 0;

                                            if (hasLatLong) {
                                                lat = latLong[0];
                                                lng = latLong[1];

                                                addMarker(new GeoPoint(lat, lng), localFile);
                                                invalidateMapView();
                                            }


                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                    } catch (IOException e) {
                    }
                }
            }
        });


    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Context context = this.getActivity();

        assert context != null;
        createLocation(context);
    }

    private void createLocation(Context context) {
        MyLocationNewOverlay mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(context), mMapView);
        mLocationOverlay.enableMyLocation();
        mLocationOverlay.enableFollowLocation();
        mMapView.getOverlays().add(mLocationOverlay);

        RotationGestureOverlay mRotationGestureOverlay = new RotationGestureOverlay(context, mMapView);
        mRotationGestureOverlay.setEnabled(true);
        mMapView.setMultiTouchControls(true);
        mMapView.getOverlays().add(mRotationGestureOverlay);

        mMapView.setMultiTouchControls(true);
        mMapView.setTilesScaledToDpi(true);


        mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(context), mMapView);
        mLocationOverlay.enableMyLocation();
        mMapView.getOverlays().add(mLocationOverlay);

        IMapController mapController = mMapView.getController();
        mapController.setZoom(18);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMapView.onDetach();

    }

    private void invalidateMapView() {
        mMapView.invalidate();
    }
}
